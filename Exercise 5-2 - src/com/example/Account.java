package com.example;

public abstract class Account {
	// generic account code
	protected double balance;

	public abstract String getDescription();

	public abstract boolean withdraw(double amount);

	public Account(double balance) {
		this.balance = balance;
	}

	public double getBalance() {
		return balance;
	}

	public void deposit(double amount) {
		balance += amount;
	}

	@Override
	public String toString() {
		return getDescription() + ": current balance is " + balance;
	}
}
