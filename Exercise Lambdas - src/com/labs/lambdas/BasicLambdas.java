package com.labs.lambdas;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class BasicLambdas {
	public static void main(String[] args) {
		BasicLambdas basicLamdas = new BasicLambdas();
		basicLamdas.consumer();
		basicLamdas.supplier();
		basicLamdas.predicate();
		basicLamdas.function();
	}
	
	public void consumer() {
		System.out.println("----- Part 1 - Question 1. -----");
		// 1a. Anonymous Inner Class
		Printable<String> anonInnerClass = new Printable<String>() {
			@Override
			public void print(String t) {
				System.out.println(t);
			}
		};
		anonInnerClass.print("Anonymous inner class print");
		
		// 1b. Lambda
		Printable<String> printableLambda = s -> System.out.println(s);
		printableLambda.print("Java 8 Lambda: printable");
		
		// 1c. Consumer
		Consumer<String> consumerLamda = (s) -> System.out.println(s);
		consumerLamda.accept("Java 8 Lambda: Consumer\n");
	}
	
	public void supplier() {
		System.out.println("----- Part 1 - Question 2. -----");
		// 2a. Anonymous Inner Class
		Retrievable<Integer> anonInnerClass = new Retrievable<Integer>() {
			@Override
			public Integer retrieve() {
				return new Integer(77);
			}
		};
		System.out.println("Anonymous inner class retrieve: " + anonInnerClass.retrieve());
		
		// 2b. Lambda
		Retrievable<Integer> retrievableLambda = () -> new Integer(77);
		System.out.println("Retrievable Lambda: " + retrievableLambda.retrieve());
		
		// 2c. Supplier
		Supplier<Integer> supplierLambda = () -> new Integer(77);
		System.out.println("Supplier Lambda: " + supplierLambda.get() + "\n");
	}
	
	public void predicate() {
		System.out.println("----- Part 1 - Question 3. -----");
		// 3a. Anonymous Inner Class
		Evaluate<Integer> anonInnerClass = new Evaluate<Integer>() {
			@Override
			public boolean checkIfNegative(Integer t) {
				return t < 0;
			}
		};
		System.out.println("Anonymous inner class evaluate -1: " + anonInnerClass.checkIfNegative(-1));
		System.out.println("Anonymous inner class evaluate +1: " + anonInnerClass.checkIfNegative(1));
		
		// 3b. Lambda
		Evaluate<Integer> evaluateLambda = t -> t < 0;
		System.out.println("Evaluate Lambda -1: " + evaluateLambda.checkIfNegative(-1));
		System.out.println("Evaluate Lambda +1: " + evaluateLambda.checkIfNegative(1));
		
		// 3c. Predicate
		Predicate<Integer> predicateLambda = t -> t < 0;
		System.out.println("Evaluate Lambda -1: " + predicateLambda.test(-1));
		System.out.println("Evaluate Lambda +1: " + predicateLambda.test(1) + "\n");
		
	}
	
	public void function() {
		System.out.println("----- Part 1 - Question 4. -----");
		// 4a. Anonymous Inner Class
		Functionable<Integer, String> anonInnerClass = new Functionable<Integer, String>() {
			@Override
			public String applyThis(Integer t) {
				return "The number passed in is " + t;
			}
		};
		System.out.println("Anonymous inner class function: " + anonInnerClass.applyThis(77));
		
		//4b Lambda
		Functionable<Integer, String> functionableLambda = t -> "The number passed in is " + t;
		System.out.println("Functionable Lambda: " + functionableLambda.applyThis(77));
		
		//4c Function
		Function<Integer, String> functionLambda = t -> "The number passed in is " + t;
		System.out.println("Functionable Lambda: " + functionLambda.apply(77) + "\n");
	}
	
//	private static void sortName(List<Person>) {
//		
//	}
//	
//	private static void sortAge(List<Person>) {
//		
//	}
//	
//	private static void sortHeight(List<Person>) {
//		
//	}
//	
//	private static void sortNameUsingLambdas(List<Person>) {
//		
//	}
//	
//	private static void sortAgeUsingLambdas(List<Person>) {
//		
//	}
//	
//	private static void sortHeightUsingLambdas(List<Person>) {
//		
//	}
//	
//	private static List<Person> getPeople() {
//		 
//	}
}
