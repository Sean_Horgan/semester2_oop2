package com.example;

interface Printable<T>{
	void print(T t);
}

public class LambdaWithMethod {

	public static void main(String[] args) {
		LambdaWithMethod lambda = new LambdaWithMethod();
//		lambda.consumer();
//		lambda.supplier();
//		lambda.predicate();
//		lambda.function();
//		lambda.sortAge(lambda.getPeople());
//		lambda.sortName(lambda.getPeople());
//		lambda.sortHeight(lambda.getPeople());
//		lambda.sortAgeUsingLambdas(lambda.getPeople());
		
		
		Printable<String> printString = string -> System.out.println("sfsdfsd");
		Printable<String> printString2 = string -> System.out.println(string);
		Printable<String> printString3 = string -> System.out.println(string);
		Printable<String> printString4 = string -> System.out.println(string);
		
		lambda.printStuff(printString, "fdfs");
		lambda.printStuff(printString2, "fdfs");
		lambda.printStuff(printString3, "fdfs");
		lambda.printStuff(printString4, "fdfs");	
	}
	
	private void printStuff(Printable<String> printStuff, String currency) {
		printStuff.print(currency);
	}
}