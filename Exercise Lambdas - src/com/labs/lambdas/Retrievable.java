package com.labs.lambdas;

public interface Retrievable<T> {
	public T retrieve();
}
