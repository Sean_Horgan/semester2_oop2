package com.labs.lambdas;

public interface Evaluate<T> {
	public boolean checkIfNegative(T t);
}
