import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CalculateMode {
	private static Scanner sc;
	public static void main(String[] args) {
		sc = new Scanner(System.in);
		Map<Integer, Integer> map = new HashMap<Integer, Integer>();
		
		System.out.print("Enter number of data items--> ");
		Integer numberOfItems = sc.nextInt();
		sc.close();
		
		System.out.println("Highest Frequency in map is " + populateMap(numberOfItems, map) + ".");
	}
	
	public static int populateMap(int numberOfItems, Map<Integer, Integer> map) {
		sc = new Scanner(System.in);
		for(int i=0; i<numberOfItems; i++) {
			System.out.print("Enter the data--> ");
			Integer submittedData = sc.nextInt();
			for(Integer key : map.keySet()) {
				if(key.intValue() == submittedData.intValue()) {
					map.put(key, map.get(key) + 1);
				} else {
					map.put(key, 1);
				}
			}
		}
		sc.close();
		return 1;
	}
}
