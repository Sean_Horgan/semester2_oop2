package com.example.solution;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
// http://stackoverflow.com/questions/27581/what-issues-should-be-considered-when-overriding-equals-and-hashcode-in-java
// Whenever a.equals(b), then a.hashCode() must be same as b.hashCode().
// If you override equals(), then you should override hashCode().
// Use the same set of fields that you use to compute equals() to compute hashCode().
class Person{
    String name;
    int age;

    public Person(String name, int age){
        this.name = name;
        this.age = age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object obj){
        return (((Person)obj).name.equals(this.name)) && 
                (((Person)obj).age == this.age);
    }
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.name);
        hash = 97 * hash + this.age;
        return hash;
    }
    
}
public class HashCodeTest {
    public static void main(String a[]){
        Person johnA = new Person("John",21);
        Person johnB = new Person("John",21);
        Person mary  = new Person("Mary",22);

        System.out.println("johnA.equals(johnB) is :"+johnA.equals(johnB));  // true
        System.out.println("johnA.equals(mary) is  :"+johnA.equals(mary));   // false
        System.out.println();
        
        Map<Person, String> people = new HashMap<>();
        // Note: johnA is "stored" in bucket X where "X = the current hashcode value"
        people.put(johnA, "Great Student");
        System.out.println("johnA "+johnA.hashCode()+ " in map? : "+people.containsKey(johnA)); // true
        
        // false (if hashcode() not overidden) - because the default hashCode() from Object
        //      is used which include the internal object reference in the hash and therefore
        //      the hashcode for johnB <> the hashcode for johnA
        // true  (if hashcode() *is* overidden) - yes, there is an object with that hashcode
        // in the Map!
        System.out.println("johnB "+johnB.hashCode()+" in map? : "+people.containsKey(johnB));  // false/true 
        System.out.println();
        
        // change the object state
        johnA.setAge(22);
        // now its false as the object state has changed i.e. the hash code is re-calculated
        // and is going to be different because the 'age' is now different - this is dangerous!
        // Now, we try to "locate". This must be done with the same hashcode as we used to store
        // earlier. However, now that we have changed the object state for johnA, johnA will go 
        // to the wrong bucket (as its hashcode is now different to the one used to store originally).
        // johnB has the correct hashcode (busket) but it will fail the equals() test because
        // johnB's age is 21 (and johnA's has been changed to 22!).
        System.out.println("johnA "+johnA.hashCode()+" in map? : "+people.containsKey(johnA)); // false, hashcode different
        System.out.println("johnB "+johnB.hashCode()+" in map? : "+people.containsKey(johnB)); // false, equals() fails
        System.out.println();
                                                    
//        // comment out the hashCode() i.e use the super version (works on object refs)
//        // now its true
//        System.out.println("johnA "+johnA.hashCode()+ " in map? : "+people.containsKey(johnA)); // true
////        System.out.println(people.containsKey(johnA)); // true
//        
//        // So, if you are going to be changing the object state of the objects, use the inherited
//        // hashCode() from Object; if not, and you are overriding equals() then override hashCode()
//        // also - override both or none of them...
        
    }
}