package com.labs.lambdas;

public interface Printable<T> {
	public void print(T t);
}
